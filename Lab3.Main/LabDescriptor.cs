﻿using Lab3.Implementation;
using System;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(ITemperatura);
        public static Type I2 = typeof(IDrzwi);
        public static Type I3 = typeof(IPrzycisk);

        public static Type Component = typeof(Mikrofalowka);

        

        public static GetInstance GetInstanceOfI1 = (Component) => Component;
        public static GetInstance GetInstanceOfI2 = (Component) => Component;
        public static GetInstance GetInstanceOfI3 = (Component) => Component;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(TryFascade);
        public static Type MixinFor = typeof(ITemperatura);

        #endregion
    }
}
