﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public interface ITemperatura
    {
        void UstawTemperature();
        int temperaturaGrzania(int tempGrzania);
    }
    public interface IDrzwi
    {
        void OtworzDrzwi();
        void ZamknijDrzwi();
    }
    public interface IPrzycisk
    {
        void Start();
        void Stop();
    }


    public static class TryFascade
    {
        public static void GrzanieWSrodku(this ITemperatura target)
        {
            Console.WriteLine("aaa");
        }
    }


    public class Mikrofalowka : ITemperatura, IDrzwi, IPrzycisk
    {
        Fasada fasada = new Fasada();
        public void UstawTemperature()
        {
            fasada.Zrobcos();
        }
        public void OtworzDrzwi()
        {
            Console.WriteLine();
        }
        public void ZamknijDrzwi()
        {
            Console.WriteLine();
        }
        public void Start()
        {
            Console.WriteLine();
        }
        public void Stop()
        {
            Console.WriteLine();
        }
        public int temperaturaGrzania(int temperatura)
        {
            return temperatura;
        }
        public Mikrofalowka()
        {
        }
        

    }
}
